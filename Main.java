import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        int[] arr = new int[4];
        Scanner sc = new Scanner(System.in);

        for (int i= 0;i < arr.length; i++){
            System.out.println("Введите число: ");
            while (!sc.hasNextInt()) {
                System.out.println("Введите число: ");
                sc.nextLine();
            }
            int value = sc.nextInt();
            arr[i] = value;
        }

        int min = arr[0];
        for (int i= 0; i < arr.length; i++ ){
            if (arr[i]<min)
                min = arr[i];
        }
        System.out.println("Minimal: " + min);

    }
}